//var express = require('express');
//var app = express();
// la barra"/" es la uri
//app.get('/',function(request,response){
//response.send('Buenos dias Lima');
//});

//app.listen(3000,function(){
//  console.log("holaaaaaaa");
//  });


//  app.listen(3000);
//  console.log("api techU escuchando en el puerto 3003");
//********
//el require es como imports
var express = require('express');
var userFile = require('./user.json');
var userLoge = require('./logeados.json');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json()); //dice que el body de i-o soporte con notacion json
var totalUsers = 0; //new2
const URL_BASE ='/apitechu/v1/';
const PORT = process.env.PORT || 3000;

  //peticion de get de todos los 'users' (collentions)
  app.get(URL_BASE + 'users',
  function(request,response){ //esto es una callback, es una funcion anonima  que aparece dentro de otra funcion
  //response.send('Buenos sdias pruebaaa');
  //console.log('GET' + URL_BASE + 'users');
  response.send(userFile);
  });

  //peticion de get de un 'users' (instancia)
  app.get(URL_BASE + 'users/:id',
  function(request,response){
  //  console.log('GET'+ URL_BASE + 'users/id');
  let indice = request.params.id;
  let instancia= userFile[indice - 1];
  //  console.log(instancia);
  let respuesta = (instancia !=undefined) ? instancia:{"mensaje":"recurso no encontrado"};
  response.status(200);
  response.send(respuesta);
  });

//peticiòn GET con QUERY String-  ejemplo hecho en clase
//app.get(URL_BASE + 'usersq',
//function(request,response) {
//  console.log('GET' + URL_BASE + 'con query string');
//  console.log(request.query);
//  response.send(respuesta);
//});

//peticion POST es para añadir un registro.
app.post(URL_BASE + 'users',
function(req,res){
  totalUsers=userFile.length;
  let newUser = {
    userID : totalUsers +1,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    password: req.body.password
  }
  userFile.push(newUser);
  res.status(200);
  res.send({"mensaje":"Usuario creado con éxito ", "Usuario" : newUser});
  }
);

//put inicio (actualizar)
app.put(URL_BASE + 'users/:ident',
function(req,res){
  //var indice = req.body.userID;
  let ident = req.params.ident;
  let instancia= userFile[ident - 1];
  let datos = Object.keys(req.body).length;
  if (datos > 0)
  {
     if (instancia != undefined)
     {
       userFile[ident-1].first_name= req.body.first_name;
       userFile[ident-1].last_name=req.body.last_name;
       userFile[ident-1].email=req.body.email;
       userFile[ident-1].password=req.body.password;
       res.status(200);
       res.send({"mensaje":"Usuario actualizado con exito ", "Usuario" : userFile[ident-1]});
     }
     else {
       res.status(200);
       res.send({"mensaje":"Usuario no existe "});
     }
  }//new
  else {
  res.status(200);
  res.send({"mensaje":"Ingrese datos a actualizar "});
}}
);
//put fin

//delete inicio
app.delete(URL_BASE + 'users/:identi',
function(req,res)
{
  let identi = req.params.identi;
  let instancia= userFile[identi - 1]; //new
  //console.log(instancia);
  if  (instancia != undefined)
  {
    userFile.splice(identi-1,1);
    res.status(200);
    res.send({"mensaje" : "Registro eliminado " + JSON.stringify(userFile)});
  }
  else {
    res.send({"mensaje":"No existe ID "});
  }

}
);

//delete fin

//loging
app.post(URL_BASE + 'login',
function(req,res){
  var usuario = req.body.email;
  var password = req.body.password;
  var escritos = 0;
  for (us of userFile)
  {
    console.log(us);
    if (us.email == usuario)
    {
      if (us.password == password)
      {
        us.logged = true;
        escritos=1;
        escribirUsuarioEnArchivo(userFile);
        //console.log("login correcto user y pass");
        res.send({"mensaje" : "login correcto" , "usuario" : us.email, "logged" : "true"});
      }
      else{
    //  console.log("login incorrecto");
      res.send({"mensaje" : "login incorrecto" , "usuario" : us.email, "logged" : "false"});
      }
    }
  }
  if (escritos ==0 )
  {
    res.send({"mensaje" : "logeo incorrecto"});
  }
});

function escribirUsuarioEnArchivo(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json",jsonUserData,"utf8",
 function(err)
  {
    if (err)
    {
      console.log(err);
    } else {
      console.log("datos escritos en archivo");

    }
  })

};
//fin loggin

//inicio logout ingresando mail y password
app.post(URL_BASE + 'logout',
function(req,res){
 var usuario = req.body.email;
 var password = req.body.password;
 var escritos = 0;
 for (us of userFile)
 {
   console.log(us);
   if (us.email == usuario)
   {
     if (us.password == password) {
       escritos=1;
       if (us.logged) {
          escritos=2;
          console.log(us.logged);
         delete us.logged;
          escribirUsuarioEnArchivo(userFile);
        //  console.log("logout correcto user y pass");
         res.send({"mensaje" : "logout correcto"});
       }
     }
   }
 }
 if (escritos ==0 )
 {
   res.send({"mensaje" : "Logout incorrecto"});
 }
 else {
   if  (escritos ==1 ){
     res.send({"mensaje" : "Logout Incorrecto"});
   }
 }

});


//fin logout


//inicio logout usando el ID
app.post(URL_BASE + 'logout/:id',
function(req,res){
  let id = req.params.id;
  let instancia = userFile[id-1];
  if (instancia != undefined)
  {
    var logg = userFile[id-1].logged;
    console.log(logg);
    if (logg)
    {
      delete instancia.logged;
      escribirUsuarioEnArchivo(userFile);
    //  console.log("logout correcto por id");
     res.send({"mensaje" : "logout correcto" , "id" : userFile[id-1]});
    }
    else {
      //console.log("logout incorrecto por id");
     res.send({"mensaje" : "Loout incorrecto"});
    }
  }
});

//fin logout id

//ini usuarios logeados con query string grabando en otro archivo logeados.jon
app.post(URL_BASE + 'logeados',
function(req,res){
  var escritos = 0;
  let arreglo = [];
  for (us of userFile){
    if (us.logged){
      //console.log(us);
      arreglo.push(us);
      escritos = escritos + 1;
    }
  }
  escribirUsuariosLogeados(arreglo);
  //console.log(escritos);
  res.send({"Total escritos" : escritos});
});

// function escribir en nuevo archivo.

function escribirUsuariosLogeados(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./logeados.json",jsonUserData,"utf8",
 function(err)
  {
    if (err)
    {
      console.log(err);
    } else {
      console.log("datos escritos en archivo");

    }
  })

};


//fin usuarios logeados

//nuevo logeados con GET
app.get(URL_BASE + 'logueados2',
      function(req,res)
      {
        var logados=new Array();
        for(user of userFile)
        {
          if(user.logged)
          {
            logados.push(user);
          }
        }
        res.send(logados);
      }
    );

//fin logeados con get

//inicio GET con query string, los campos que deben ingresar son idIni / idFin
app.get(URL_BASE + 'rangos',
function(request,response) {
  var idIni = 0;
  var idFin = 0;
  var total = userFile.length;
  var i = 0;
  let newArreglo =new Array();
  console.log('GET' + URL_BASE + 'con query string');
  idIni=parseInt(request.query.idIni,10);
  idFin=parseInt(request.query.idFin,10);
  if ( isNaN(idIni) || isNaN(idFin) ){
    response.send({"Mensaje": "Debe ingresar números, corrija"});
  }else if ( (idIni<=0) || (idFin <=0) ){
    response.send({"Mensaje": "Rango no debe ser menor a 1"});
  }else if ((idIni > total) || (idFin > total) ){
    response.send({"Mensaje": "Uno o ambos valores fuera del rango, corrija"});
  }else if (idIni > idFin)  {
    response.send({"Mensaje": "Valor inicial no puede ser mayor que valor final, corrija"});
  }else {
    for (i=idIni; i<=idFin; i++){
      console.log(userFile[i-1]);
      newArreglo.push(userFile[i-1]);
    }
    response.send({"Mensaje": "Éxito !!", "Listado": JSON.stringify(newArreglo)});
  }
  response.send({"Mensaje": "Fin del listado"});
});
//fin query string
  app.listen(3000,function(){
  console.log("api techU escuchando en el puerto 3000 new");
  });
